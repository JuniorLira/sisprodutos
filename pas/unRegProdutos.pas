unit unRegProdutos;

interface

uses
 Winapi.Windows, Winapi.Messages, System.SysUtils, Vcl.Forms;

type

 TRegProdutos = class

  private
    FVlrAtc: Double;
    FCdNCM: String;
    FVlrCom: Double;
    FVlrVis: Double;
    FQtdMin: Double;
    FDescr: String;
    FUnidade: String;
    FVlrPrz: Double;
    FSaldo: Double;
    FCdBar: String;
    FCodigo: integer;
    procedure SetCdBar(const Value: String);
    procedure SetCdNCM(const Value: String);
    procedure SetDescr(const Value: String);
    procedure SetQtdMin(const Value: Double);
    procedure SetSaldo(const Value: Double);
    procedure SetUnidade(const Value: String);
    procedure SetVlrAtc(const Value: Double);
    procedure SetVlrCom(const Value: Double);
    procedure SetVlrPrz(const Value: Double);
    procedure SetVlrVis(const Value: Double);
    procedure SetCodigo(const Value: integer);
 public
  property Codigo : integer read FCodigo write SetCodigo;
  property Descr : String read FDescr write SetDescr;
  property CdNCM : String read FCdNCM write SetCdNCM;
  property CdBar : String read FCdBar write SetCdBar;
  property Unidade : String read FUnidade write SetUnidade;
  property VlrCom : Double read FVlrCom write SetVlrCom;
  property VlrVis : Double read FVlrVis write SetVlrVis;
  property VlrPrz : Double read FVlrPrz write SetVlrPrz;
  property VlrAtc : Double read FVlrAtc write SetVlrAtc;
  property Saldo  : Double read FSaldo write SetSaldo;
  property QtdMin : Double read FQtdMin write SetQtdMin;
  Constructor Create;
  destructor Destroy;
  function AddProduct : Boolean;
  function SelectProduct : Boolean;
  function EditProduct : Boolean;
  function DeleteProduct : Boolean;
 end;

implementation

uses
 unDmProdutos, unCadProdutos;

{ TRegProdutos }

function TRegProdutos.AddProduct : Boolean;
 begin
  try
   with DmProduto do
    begin
     if not(qryProdutos.Active) then
       qryProdutos.Open();
     qryProdutos.Insert;
     qryProdutosDESCRICAO.AsString := Descr;
     qryProdutosCOD_NCM.AsString := CdNCM;
     qryProdutosCOD_BARRA.AsString := CdBar;
     qryProdutosUNIDADE.AsString := Unidade;
     qryProdutosDT_CADASTRO.AsDateTime := Date;
     qryProdutosVLR_COMPRA.AsCurrency := VlrCom;
     qryProdutosVLR_VEND_VIST.AsCurrency:= VlrVis;
     qryProdutosVLR_VEND_PRAZ.AsCurrency:= VlrPrz;
     qryProdutosVLR_VEND_ATAC.AsCurrency:= VlrAtc;
     qryProdutosSALDO.AsFloat := Saldo;
     qryProdutosQTD_MINIMA.AsFloat := QtdMin;
     qryProdutos.Post;
     qryProdutos.Close;
    end;
   Result := True;
  except
   DmProduto.qryProdutos.Cancel;
   Result := False;
  end;
 end;

constructor TRegProdutos.Create;
 begin
  inherited Create;
 end;

function TRegProdutos.DeleteProduct: Boolean;
 begin
  try
   DmProduto.qryProdutos.Delete;
   Result := True;
  except
   DmProduto.qryProdutos.Cancel;
   Result := False;
  end;
 end;

destructor TRegProdutos.Destroy;
  begin
   inherited;
  end;

function TRegProdutos.SelectProduct : Boolean;
 begin
  try
   with DmProduto do
    begin
     qryProdutos.Close;
     qryProdutos.SQL.Clear;
     qryProdutos.SQL.Add('Select * From Produtos ');

     if Codigo > 0 then
      qryProdutos.SQL.Add('where ID = ' + IntToStr(Codigo))
     else if Descr <> '' then
      qryProdutos.SQL.Add('where Descricao Starting ' + QuotedStr(Descr))
     else if CdBar <> '' then
      qryProdutos.SQL.Add('where Cod_barra = ' + QuotedStr(CdBar));

     qryProdutos.Open();
    end;
   Result := True;
  except
   Result := False;
  end;
 end;

procedure TRegProdutos.SetCdBar(const Value: String);
 begin
  FCdBar := Value;
 end;

procedure TRegProdutos.SetCdNCM(const Value: String);
 begin
  FCdNCM := Value;
 end;

procedure TRegProdutos.SetCodigo(const Value: integer);
 begin
  FCodigo := Value;
 end;

procedure TRegProdutos.SetDescr(const Value: String);
 begin
  FDescr := Value;
 end;

procedure TRegProdutos.SetQtdMin(const Value: Double);
 begin
  FQtdMin := Value;
 end;

procedure TRegProdutos.SetSaldo(const Value: Double);
 begin
  FSaldo := Value;
 end;

procedure TRegProdutos.SetUnidade(const Value: String);
 begin
  FUnidade := Value;
 end;

procedure TRegProdutos.SetVlrAtc(const Value: Double);
 begin
  FVlrAtc := Value;
 end;

procedure TRegProdutos.SetVlrCom(const Value: Double);
 begin
  FVlrCom := Value;
 end;

procedure TRegProdutos.SetVlrPrz(const Value: Double);
 begin
  FVlrPrz := Value;
 end;

procedure TRegProdutos.SetVlrVis(const Value: Double);
 begin
  FVlrVis := Value;
 end;

function TRegProdutos.EditProduct: Boolean;
 begin
  try
   with DmProduto do
    begin
     if not(qryProdutos.Active) then
       qryProdutos.Open();
     qryProdutos.Edit;
     qryProdutosDESCRICAO.AsString := Descr;
     qryProdutosCOD_NCM.AsString := CdNCM;
     qryProdutosCOD_BARRA.AsString := CdBar;
     qryProdutosUNIDADE.AsString := Unidade;
     qryProdutosVLR_COMPRA.AsCurrency := VlrCom;
     qryProdutosVLR_VEND_VIST.AsCurrency:= VlrVis;
     qryProdutosVLR_VEND_PRAZ.AsCurrency:= VlrPrz;
     qryProdutosVLR_VEND_ATAC.AsCurrency:= VlrAtc;
     qryProdutosSALDO.AsFloat := Saldo;
     qryProdutosQTD_MINIMA.AsFloat := QtdMin;
     qryProdutos.Post;
     qryProdutos.Close;
    end;
   Result := True;
  except
   DmProduto.qryProdutos.Cancel;
   Result := False;
  end;
 end;

end.
