program SisProdutos;

uses
  Vcl.Forms,
  unProdutos in '..\pas\unProdutos.pas' {frmProduto},
  unDmProdutos in '..\pas\unDmProdutos.pas' {DmProduto: TDataModule},
  unRegProdutos in '..\pas\unRegProdutos.pas',
  unCadProdutos in '..\pas\unCadProdutos.pas' {frmCadProduto},
  unConexao in '..\pas\unConexao.pas' {frmConexao};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'Sistema de Produtos';
  Application.CreateForm(TDmProduto, DmProduto);
  Application.CreateForm(TfrmProduto, frmProduto);
  Application.Run;
end.
