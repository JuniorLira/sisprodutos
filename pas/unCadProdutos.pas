unit unCadProdutos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Mask, Vcl.StdCtrls,
  Vcl.ComCtrls, Vcl.Buttons;

type
  TfrmCadProduto = class(TForm)
    edtCodigo: TEdit;
    edtCodNCM: TEdit;
    edtCodBarra: TEdit;
    edtDescricao: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    btnConfirmar: TBitBtn;
    Label6: TLabel;
    btnCancelar: TBitBtn;
    cbUnidade: TComboBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Bevel1: TBevel;
    edtVlrCompra: TEdit;
    edtVlrVista: TEdit;
    edtVlrPrazo: TEdit;
    edtVlrAtacado: TEdit;
    edtSaldo: TEdit;
    edtDtCadastro: TMaskEdit;
    edtQtdMinima: TEdit;
    procedure KeyEditsNumbers(Sender: TObject; var Key: Char);
    procedure KeyEditCode(Sender: TObject; var Key: Char);
    procedure btnConfirmarClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    NewRecord : Boolean;
  public
    { Public declarations }
  end;

var
  frmCadProduto: TfrmCadProduto;

implementation

{$R *.dfm}

uses unRegProdutos;

{ TfrmCadProduto }

procedure TfrmCadProduto.btnConfirmarClick(Sender: TObject);
var TCadProduto : TRegProdutos;
 begin
  try
   TCadProduto := TRegProdutos.Create;
   TCadProduto.Descr  := edtDescricao.Text;
   TCadProduto.CdNCM  := edtCodNCM.Text;
   TCadProduto.CdBar  := edtCodBarra.Text;
   TCadProduto.Unidade:= cbUnidade.Text;
   TCadProduto.VlrCom := StrToFloat(edtVlrCompra.Text);
   TCadProduto.VlrVis := StrToFloat(edtVlrVista.Text);
   TCadProduto.VlrPrz := StrToFloat(edtVlrPrazo.Text);
   TCadProduto.VlrAtc := StrToFloat(edtVlrAtacado.Text);
   TCadProduto.Saldo  := StrToFloat(edtSaldo.Text);
   TCadProduto.QtdMin := StrToFloat(edtQtdMinima.Text);
   if (NewRecord) then
    begin
     if TCadProduto.AddProduct then
      Application.MessageBox('Produto cadastrado com sucesso!','Dados Gravados', MB_OK + MB_ICONINFORMATION)
     else
      Application.MessageBox('Erro ao tentar cadastrar um produto!','Erro', MB_OK + MB_ICONERROR);
    end
   else
    begin
     if TCadProduto.EditProduct then
      Application.MessageBox('Produto alterado com sucesso!','Dados Gravados', MB_OK + MB_ICONINFORMATION)
     else
      Application.MessageBox('Erro ao tentar alterar um produto!','Erro', MB_OK + MB_ICONERROR);
    end;
  finally
   TCadProduto.Destroy;
  end;
 end;

procedure TfrmCadProduto.FormKeyPress(Sender: TObject; var Key: Char);
 begin
  //Tab Order pelo Enter.
  if (Key = #13) then
   begin
    key := #0;
    Perform(WM_NEXTDLGCTL,0,0);
   end;
 end;

procedure TfrmCadProduto.FormShow(Sender: TObject);
 begin
  NewRecord := (Caption = 'Cadastro de Produto - Incluindo');
  if NewRecord then
   edtDtCadastro.EditText := DateToStr(Date);
 end;

procedure TfrmCadProduto.KeyEditCode(Sender: TObject; var Key: Char);
 begin
  if  not(Key in ['0'..'9',#8]) then
   Key := #0;
 end;

procedure TfrmCadProduto.KeyEditsNumbers(Sender: TObject; var Key: Char);
 begin
  if  not(Key in ['0'..'9',',',#8]) then
   Key := #0;
 end;

end.
