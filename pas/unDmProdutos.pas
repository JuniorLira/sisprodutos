unit unDmProdutos;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet;

type
  TDmProduto = class(TDataModule)
    Conexao: TFDConnection;
    dsProdutos: TDataSource;
    qryProdutos: TFDQuery;
    qryProdutosID: TIntegerField;
    qryProdutosCOD_BARRA: TStringField;
    qryProdutosDESCRICAO: TStringField;
    qryProdutosCOD_NCM: TStringField;
    qryProdutosUNIDADE: TStringField;
    qryProdutosDT_CADASTRO: TDateField;
    qryProdutosVLR_COMPRA: TCurrencyField;
    qryProdutosVLR_VEND_VIST: TCurrencyField;
    qryProdutosVLR_VEND_PRAZ: TCurrencyField;
    qryProdutosVLR_VEND_ATAC: TCurrencyField;
    qryProdutosSALDO: TCurrencyField;
    qryProdutosQTD_MINIMA: TCurrencyField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DmProduto: TDmProduto;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

end.
