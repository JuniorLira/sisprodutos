unit unProdutos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.ToolWin, Vcl.ComCtrls,
  Vcl.StdCtrls, Data.DB, Vcl.Buttons, Vcl.Grids, Vcl.DBGrids;

type
  TfrmProduto = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    pgcControls: TPageControl;
    TabPesquisa: TTabSheet;
    TabCadastro: TTabSheet;
    dbgProdutos: TDBGrid;
    GroupBox1: TGroupBox;
    edtLocalizar: TEdit;
    rgTipoPesquisa: TRadioGroup;
    btnLocalizar: TSpeedButton;
    btnIncluir: TSpeedButton;
    btnAlterar: TSpeedButton;
    btnExcluir: TSpeedButton;
    btnSair: TSpeedButton;
    stbInformacoes: TStatusBar;
    TabConfig: TTabSheet;
    btnConfigDados: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnIncluirClick(Sender: TObject);
    procedure btnLocalizarClick(Sender: TObject);
    procedure edtLocalizarKeyPress(Sender: TObject; var Key: Char);
    procedure btnAlterarClick(Sender: TObject);
    procedure dbgProdutosDblClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure btnConfigDadosClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmProduto: TfrmProduto;

implementation

{$R *.dfm}

uses unDmProdutos, unCadProdutos, unRegProdutos, unConexao;

procedure TfrmProduto.btnAlterarClick(Sender: TObject);
 begin
  if not(DmProduto.qryProdutos.Active) or (DmProduto.qryProdutos.IsEmpty) then
   begin
    Application.MessageBox('Tabela Fechada ou vazia, selecione um registro antes de Alterar!',
                           'Tabela sem registros', MB_OK +MB_ICONWARNING);
    Abort;
   end;

  if not(dbgProdutos.Focused) then
   begin
    Application.MessageBox('Selecione um registro antes de Alterar!',
                           'Erro de Sele��o', MB_OK +MB_ICONWARNING);
    Abort;
   end;

  Application.CreateForm(TfrmCadProduto, frmCadProduto);
  with frmCadProduto, DmProduto do
   begin
    Caption := 'Cadastro de Produto - Alterando';
    edtCodigo.Text := qryProdutosID.AsString;
    edtDescricao.Text := qryProdutosDESCRICAO.AsString;
    edtCodBarra.Text := qryProdutosCOD_BARRA.AsString;
    edtCodNCM.Text := qryProdutosCOD_NCM.AsString;
    cbUnidade.Style := csDropDown;
    cbUnidade.Text := qryProdutosUNIDADE.AsString;
    edtDtCadastro.EditText := DateToStr(qryProdutosDT_CADASTRO.AsDateTime);
    edtVlrCompra.Text := qryProdutosVLR_COMPRA.AsString;
    edtVlrVista.Text := qryProdutosVLR_VEND_VIST.AsString;
    edtVlrPrazo.Text := qryProdutosVLR_VEND_PRAZ.AsString;
    edtVlrAtacado.Text := qryProdutosVLR_VEND_ATAC.AsString;
    edtSaldo.Text := qryProdutosSALDO.AsString;
    edtQtdMinima.Text := qryProdutosQTD_MINIMA.AsString;
    ShowModal;
   end;
  FreeAndNil(frmCadProduto);
 end;

procedure TfrmProduto.btnConfigDadosClick(Sender: TObject);
var
  FileConnection : String;
 begin
  try
   with DmProduto do
    begin
     if (Conexao.Connected) then
      Conexao.Connected := False;

     ShowConexao(FileConnection);
     Conexao.Params[0] := 'Database='+FileConnection;
     Conexao.Connected := True;
    end;
  except
   Application.MessageBox('Erro ao tentar estabelecer uma conexao com o banco de dados!',
                          'Erro', MB_OK + MB_ICONERROR);
  end;

  if (DmProduto.Conexao.Connected) then
   stbInformacoes.Panels[3].Text := 'Status da Conex�o: Conectado'
  else
   stbInformacoes.Panels[3].Text := 'Status da Conex�o: Desconectado';
  stbInformacoes.Panels[2].Text :=  DmProduto.Conexao.Params[0];
 end;

procedure TfrmProduto.btnExcluirClick(Sender: TObject);
var
  TDeleteProduto : TRegProdutos;
 begin
  if not(DmProduto.qryProdutos.Active) or (DmProduto.qryProdutos.IsEmpty) then
   begin
    Application.MessageBox('Tabela Fechada ou vazia, selecione um registro antes de tentar excluir!',
                           'Tabela sem registros', MB_OK +MB_ICONWARNING);
    Abort;
   end;

  if not(dbgProdutos.Focused) then
   begin
    Application.MessageBox('Selecione um registro antes de tentar excluir!',
                           'Erro de Sele��o', MB_OK +MB_ICONWARNING);
    Abort;
   end;

  if(Application.MessageBox('Deseja realmente excluir o registro selecionado?',
                            'Excluir Item', MB_YESNO + MB_ICONQUESTION) = IdYes) then
   try
    TDeleteProduto := TRegProdutos.Create;
    if (TDeleteProduto.DeleteProduct) then
     Application.MessageBox('Produto excluido com sucesso!', 'Item Excluido', MB_OK + MB_ICONINFORMATION)
    else
     Application.MessageBox('Erro ao tentar excluir um item da tabela de produto!','Erro', MB_OK + MB_ICONERROR);
   finally
    TDeleteProduto.Destroy;
   end;

 end;

procedure TfrmProduto.btnIncluirClick(Sender: TObject);
 begin
  Application.CreateForm(TfrmCadProduto, frmCadProduto);
  frmCadProduto.Caption := 'Cadastro de Produto - Incluindo';
  frmCadProduto.ShowModal;
  FreeAndNil(frmCadProduto);
 end;

procedure TfrmProduto.btnLocalizarClick(Sender: TObject);
Var
  TSelectProduto : TRegProdutos;
 begin
  try
   TSelectProduto := TRegProdutos.Create;
   if (rgTipoPesquisa.ItemIndex = 0) then
    begin
     if (edtLocalizar.Text = '') then
      TSelectProduto.Codigo := 0
     else
      TSelectProduto.Codigo := StrToInt(edtLocalizar.Text);
    end
   else if (rgTipoPesquisa.ItemIndex = 1) then
    TSelectProduto.Descr := edtLocalizar.Text
   else
    TSelectProduto.CdBar := edtLocalizar.Text;

   if not(TSelectProduto.SelectProduct) then
    Application.MessageBox('Erro ao tentar realizar um pesquisa na tabela de produto!','Erro', MB_OK + MB_ICONERROR);
  finally
   TSelectProduto.Destroy;
  end;
 end;

procedure TfrmProduto.btnSairClick(Sender: TObject);
 begin
  Close;
 end;

procedure TfrmProduto.dbgProdutosDblClick(Sender: TObject);
 begin
  btnAlterarClick(nil);
 end;

procedure TfrmProduto.edtLocalizarKeyPress(Sender: TObject; var Key: Char);
 begin
  if (rgTipoPesquisa.ItemIndex <> 1) then
   if  not(Key in ['0'..'9',#8]) then
    Key := #0;
 end;

procedure TfrmProduto.FormClose(Sender: TObject; var Action: TCloseAction);
 begin
  if (Application.MessageBox('Deseja realmente sair do Sistema?','Encerrar Aplica��o',
                             MB_YESNO + MB_ICONQUESTION) = mrYes) then
   Action := caFree
  else
   Action := caNone;
 end;

procedure TfrmProduto.FormCreate(Sender: TObject);
var
  FileConnection : string;
 begin
  FileConnection := '';
  with DmProduto do
   begin
    try
     if FileExists('C:\Produtos\Dados\DADOS.FDB') then
      Conexao.Connected
     else
      begin
      if (Application.MessageBox('Aten��o a base de dados padr�o n�o foi encontrada. Deseja '+
                                 'inform�-la agora?','Sem Conexao de Dados', MB_YESNO
                                 + MB_ICONQUESTION) = idYes) then
       begin
        ShowConexao(FileConnection);
        Conexao.Params[0] := 'Database='+FileConnection;
        Conexao.Connected := True;
       end;
      end;
    except
     Application.MessageBox('Erro ao tentar estabelecer uma conexao com o banco de dados!',
                            'Erro', MB_OK + MB_ICONERROR);
    end;
   end;
  Caption := Application.Title;
  pgcControls.ActivePage := TabPesquisa;
  stbInformacoes.Panels[0].Text := Application.Title;
  stbInformacoes.Panels[1].Text := 'Local do Execut�vel: ' + Application.ExeName;
  stbInformacoes.Panels[2].Text :=  DmProduto.Conexao.Params[0];
  if (DmProduto.Conexao.Connected) then
   stbInformacoes.Panels[3].Text := 'Status da Conex�o: Conectado'
  else
   stbInformacoes.Panels[3].Text := 'Status da Conex�o: Desconectado';
  stbInformacoes.Panels[4].Text := 'Data Atual: ' + DateToStr(Date);
  stbInformacoes.Panels[5].Text := 'Desenvolvido por: J�nior Lira';
 end;

end.
