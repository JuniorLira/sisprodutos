unit unConexao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Buttons;

type
  TfrmConexao = class(TForm)
    edtFile: TEdit;
    btnLocal: TButton;
    BitBtn1: TBitBtn;
    Bevel1: TBevel;
    Label1: TLabel;
    dlgBase: TOpenDialog;
    procedure btnLocalClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  function ShowConexao(var FileConnection : String) : boolean;

var
  frmConexao: TfrmConexao;

implementation

{$R *.dfm}

function ShowConexao(var FileConnection : String) : boolean;
 begin
  with TfrmConexao.Create(Application) do
   try
    Result := ShowModal = mrYes;
   finally
    if Result then
     FileConnection := edtFile.Text;
    Free;
   end;
 end;

procedure TfrmConexao.BitBtn1Click(Sender: TObject);
 begin
  if (edtFile.Text = '') then
   begin
    Application.MessageBox('Informe o caminho do banco de dados antes de continuar!',
                           'Aten��o!',MB_OK + MB_ICONEXCLAMATION);
    edtFile.SetFocus;
    Abort;
   end;
 end;

procedure TfrmConexao.btnLocalClick(Sender: TObject);
 begin
  dlgBase.Execute();
  edtFile.Text := dlgBase.FileName;
 end;

end.
